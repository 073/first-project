CREATE TABLE IF NOT EXISTS contact(
	id BIGINT PRIMARY KEY NOT NULL,
	first_name varchar(50) NOT NULL,
	last_name varchar(50) NOT NULL,
	email varchar(50) NOT NULL,
	phone varchar(15) NOT NULL
);
