package edu.imega.view;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JTable;

import edu.imega.controller.ContactController;
import edu.imega.domain.Contact;

public class Main extends JFrame {

	private static final long serialVersionUID = 6596890196521680421L;

	public Main() {
		setSize(500, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initComponents();
		setVisible(true);
	}

	private void initComponents() {
		List<Contact> temp = ContactController.getInstance().list();
		Object[][] o = new Object[temp.size()][];
		Contact c = null;
		for (int i = 0; i < o.length; i++) {
			c = temp.get(i);
			o[i] = new Object[] { c.getId(), c.getFirstName(), c.getLastName(),
					c.getEmail(), c.getPhone() };
		}

		JTable table = new JTable(o, new Object[] { "ID", "First Name",
				"Last Name", "Email", "Phone" });
		this.add(table);
	}

}
