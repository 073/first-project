package edu.imega.db;

import java.sql.Connection;
import java.sql.SQLException;

import org.h2.jdbcx.JdbcConnectionPool;
import org.h2.tools.Console;

public class DB {

	private static String username;
	private static String password;
	private static String url;
	private static JdbcConnectionPool cp;
	private Console console;

	private static DB instance;

	public static void init() {
		instance = new DB();
	}

	public void runWebConsole() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					console = new Console();
					console.runTool(new String[] { "-url", url, "-user",
							username, "-password", password });
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	private DB() {
		cp = JdbcConnectionPool.create(url, username, password);
		cp.setMaxConnections(3);
		runWebConsole();
	}

	public Connection getConnection() throws SQLException {
		return cp.getConnection();
	}

	public static DB getInstance() {
		return instance;
	}

	public String getUsername() {
		return username;
	}

	public static void setUsername(String username) {
		DB.username = username;
	}

	public String getPassword() {
		return password;
	}

	public static void setPassword(String password) {
		DB.password = password;
	}

	public String getUrl() {
		return url;
	}

	public static void setUrl(String url) {
		DB.url = url;
	}
}
