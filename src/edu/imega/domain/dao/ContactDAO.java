package edu.imega.domain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import edu.imega.db.DB;
import edu.imega.domain.Contact;

public class ContactDAO {

	private Contact contact;
	private static final String sql = "SELECT * FROM contact WHERE id=?";

	public ContactDAO(Contact contact) {
		this.contact = contact;
	}

	public Contact save() {

		return this.contact;
	}

	public boolean delete() {
		return false;
	}

	public Contact update() {
		return null;
	}

	public Contact load() {
		Contact contact = this.contact;
		try (Connection c = DB.getInstance().getConnection();
				PreparedStatement ps = c.prepareStatement(sql);) {
			ps.setLong(1, contact.getId());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				contact.setEmail(rs.getString("email"));
				contact.setFirstName(rs.getString("first_name"));
				contact.setLastName(rs.getString("last_name"));
				contact.setPhone(rs.getString("phone"));
			} else {
				contact = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return contact;
	}

	public List<Contact> list() {
		return null;
	}
}
