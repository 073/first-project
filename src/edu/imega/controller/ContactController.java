package edu.imega.controller;

import java.util.ArrayList;
import java.util.List;

import edu.imega.domain.Contact;

public class ContactController {

	static ContactController instance = new ContactController();

	private ContactController() {
	}

	public List<Contact> search(String query) {
		List<Contact> list = new ArrayList<>();
		return list;
	}

	public Contact load(long id) {
		return new Contact(id).db().load();
	}

	public List<Contact> list() {
		return new Contact().db().list();
	}

	public Contact save(Contact contact) {
		return contact.db().save();
	}

	public Contact update(long id) {
		return null;
	}

	public boolean delete(long id) {
		return false;
	}

	public static ContactController getInstance() {
		return instance;
	}

}
