package edu.imega;

import javax.swing.SwingUtilities;

import edu.imega.db.DB;

public class Main {
	public static void main(String[] args) {
		DB.setPassword("pass");
		DB.setUsername("username");
		DB.setUrl("jdbc:h2:mem:project-first;MVCC=TRUE;DB_CLOSE_ON_EXIT=FALSE;INIT=RUNSCRIPT FROM 'classpath:init.sql'");
		DB.init();

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new edu.imega.view.Main();
			}
		});
	}
}
